import { Injectable, Input } from '@angular/core';
import { HttpRequest , HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from './student.class';
import { SessionStorageService } from "ngx-webstorage";
import { Router } from "@angular/router";
import { Class } from './class.class';
import swal from 'sweetalert2'
import * as $ from 'jquery-easing';

@Injectable({
  providedIn: 'root'
})
export class RpdService {
  studentList : Student[] = [];
  studentListForClass : Student[] = [];
  profile = null;
  images = [];
  loggedUserId = null;
  videos = [];
  notifications = [];
  classes : Class[] = [];
  selectedClass : Class = null;
  serverUrl = 'http://ec2-35-182-16-173.ca-central-1.compute.amazonaws.com:8080';
  constructor(private httpClient : HttpClient,private session : SessionStorageService,private router : Router) {
    this.loggedUserId = localStorage.getItem('user');
    this.getAllStudents();
    this.getProfileAfterTime()
    
  }

  getClassFromArray(classID){
    // console.log(classID)
    return this.classes.find(c => {
      return c.id == classID
    });
  }

  getProfileAfterTime(){
    setTimeout(()=>{setInterval( () => {
      this.getProfile()},20000)
  },5000)
  }
  
  resetPassword(token,email,password){
    // console.log('reseting password')
    this.httpClient.post(`${this.serverUrl}/resetPassword`,{password},{
      headers : new HttpHeaders().set('Authorization',`Bearer ${token}`)
    })
    .toPromise()
    .then((result) => {
      this.successMessage('password has been successfully changed');
      this.logIn({email,password})
      this.switchComponentByUrl(' ');
    }).catch((err) => {
      this.errorMessage('failed to change the password');
      // console.log(err);
    });
  }

  async verifyToken(token){
    let verified = null;
    await this.httpClient.post(`${this.serverUrl}/verifyToken`,{token},{
      headers : new HttpHeaders().set('Authorization',`Bearer ${token}`)
    })
    .toPromise()
    .then((result : any) => {
      verified = result;
      console.log(result);
    }).catch((err) => {
      // console.log('failed to ',err)
      verified = null;
    });
    return verified;
  }

  registerProgram(){
    this.switchComponentByUrl('/contact');
  }

  getStudentFromArray(studentId){
    return this.studentList.find(student => student.id == studentId);
  }

  getHeaders(){
    return new HttpHeaders().set('Authorization',`Bearer ${localStorage.getItem('token')}`);
  }

  async showLoggedUserProfile(){
    this.profile = null;
    await this.getProfile();
    this.switchComponentByUrl('/students/profile')
  }

  logOut(){
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('role');
    document.location.reload();  
  }

  isAuthenticated(){
    return localStorage.getItem('token') || false;
  }

  isAdmin(){
    return localStorage.getItem('role') == 'RPDadmin' || false;
  }

  showElements(document,elements){
    // console.log(elements)
    document.on('scroll',() => {
      elements.each(function(){
      if(document.delay(500).scrollTop() >= $(this).offset().top/2-100){
        $(this).animate({
          opacity : 1,
          "margin-top" : 0
        },2000)
      }})
    })    
  }

  async getAllStudents(){
    $('.spinner').show(); 
    await this.httpClient.request(new HttpRequest("GET",`${this.serverUrl}/getStudentList`,{
      headers : this.getHeaders()
    }))
    .toPromise()
    .then(async (response : any) => {
      let newStudents = [];
      for(let student of response.body.students){
        if(!this.getStudentFromArray(student._id)){
          newStudents.push(student);
          // console.log('no passed')
        }
        // console.log(student)
      }
      for(let student of newStudents) {
        student.enrolledClasses = await <any>(this.getClassesById(student.enrolledClasses));
        // console.log(student.enrolledClasses)
      }
      this.studentList = this.studentList.concat(this.cloneStudentList(newStudents));
    }).catch((err) => {
      // console.log("error geting student list");
    });

    for (const c of this.classes) {
      if(c && c.studentList[0] && !(c.studentList[0].fName)){
        c.studentList = await this.getStudentsByIdForClass(c.studentList);
      }
    }

    $('.spinner').hide(1000); 
  }

  switchComponentByUrl(url){
    this.router.navigateByUrl(url);
  }

  getLogInData(){
    swal({
      imageUrl : '../../assets/images/logoBlack.png',
      imageWidth : 150,
      allowOutsideClick : false,
      html: '<input id="email" placeholder="Email" type="email" class="swal2-input">'+
            '<input id="password" type="password" placeholder="password" class="swal2-input">'+
            '<input id="forgotPassword" type="checkbox" class="text-info" >Forgot Password ?</input>',
      showCancelButton : true,
      showLoaderOnConfirm : true,
      confirmButtonText : "Log in",
      confirmButtonColor : 'green',
      cancelButtonColor : 'red',
      showCloseButton: true,
      preConfirm : () => {
        return new Promise(async (resolve,reject) => {
          let email = (<HTMLInputElement>document.getElementById('email')).value;
          let password = (<HTMLInputElement>document.getElementById('password')).value;
          let forgotPassword = (<HTMLInputElement>document.getElementById('forgotPassword'));
          if(forgotPassword.checked){
            this.forgotPassword();
          }
          else if(!email || !password){
           this.getLogInData();
          }
          else{
            let logInData = {email,password}
             this.logIn(logInData);
          }
        })
      }
    })
  }

  async checkStudentExist(studentEmail){
    let exists = null;
    await this.httpClient.post(`${this.serverUrl}/checkEmailExists`,{email : studentEmail})
    .toPromise()
    .then((result) => {
      // console.log('result for checking email is ',result)
      exists = result;
    }).catch((err) => {
      // console.log('error has occured while checking if student exists or not ',err)
    });
    return exists;
  }

  forgotPassword(){
    swal.close();
    swal({
      type : 'question' ,
      allowOutsideClick : false,
      title : 'Forgot your password',
      input : 'email',
      showLoaderOnConfirm : true,
      inputValidator : async (value) => {
        if(!value || !/[a-z]*[0-9]*@[a-z]+\.[a-z]+/.test(value) || !(await this.checkStudentExist(value)))
          return 'please enter a valid email address';
      },
      preConfirm : async (value) => {
        await this.getResetLink(value);
      }
    })
  }

  getResetLink(email){
    this.httpClient.post(`${this.serverUrl}/getResetLink`,{email})
    .toPromise()
    .then((result) => {
      // console.log(result);
    }).catch((err) => {
      this.errorMessage('unable to send the email, please try again');
    });
  }

  logIn(logInData){
    this.httpClient.post(`${this.serverUrl}/student/logIn`,logInData)
    .toPromise()
    .then((result:any) => {
      localStorage.setItem('token',result.token);   
      localStorage.setItem('user',result.user.id);
      localStorage.setItem('role',result.user.role) 
      this.loggedUserId = result.user;
      //this.successMessage("you have successfully logged in");   
      document.location.reload();  
    }).catch((err) => {
      // console.log('unable to login ',err); 
      this.errorMessage("you have failed logged in");   
    });
  }

  successMessage(message){
    swal({
      title : message,
      allowOutsideClick: false,
      type : "success",
      showConfirmButton: false,
      timer : 1900
    })
  }

  errorMessage(error){
    swal({
      title : error,
      allowOutsideClick: false,
      type : 'error',
      showConfirmButton: false,
      timer : 1900
    })
  }

  markNotificationRead(notificationId){
    this.httpClient.post(`${this.serverUrl}/markNotificationRead`,{notificationId},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      // console.log('notification is marked as read',result);
    }).catch((err) => {
      // console.log(err)
    });
  }

  deleteNotification(notificationId,collectionName){
    this.httpClient.post(`${this.serverUrl}/deleteNotification`,{notificationId,collectionName},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      this.profile.notifications.splice(this.profile.notifications.findIndex(notification => notification._id == notificationId),1);
      this.successMessage('notification has been deleted');
    }).catch((err) => {
      this.errorMessage('failed to delete the notification');
    });
  }

  editStudent(updatedStudent){
    this.httpClient.post(`${this.serverUrl}/editStudent`,updatedStudent,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((response : any) => {
      this.studentList = this.cloneStudentList(response.students)
      this.successMessage(updatedStudent.fName + '\'s info has been changed');
      this.switchComponentByUrl('/students')      
    })
    .catch((reason) => {
      // console.log('can\'t change student',reason);
      this.errorMessage('Failed to change ' + updatedStudent.name + '\'s info');
    })
  }

  addStudent(student : Student){
    this.httpClient.post(`${this.serverUrl}/addStudent`,student,{
      headers : this.getHeaders()
    })
    .toPromise()
    .catch((reason) => {
      // console.log("can't add student", reason);
      this.errorMessage('Failed to add ' + student.fName + ' to the Academy');
    })
    .then((response : any) => {
      // console.log('getting student list ',response)
      this.studentList = this.cloneStudentList(response.students);
      this.successMessage(student.fName + ' has been added to the Academy');
    });
  }

  createClass(newClass : Class){
    this.httpClient.post(`${this.serverUrl}/createClass`,newClass,{
      headers : this.getHeaders()
    })
    .toPromise()
    .catch((reason) => {
      // console.log("can't create a new class", reason);
      this.errorMessage('Failed to create a new class')
    })
    .then((response : any) => {
      // console.log('response after create request', response);
      this.classes.push(new Class().clone(response.newClass));
      this.successMessage(newClass.name + ' class has been created');
    });
  }
 
  saveUpdatedStudentClassList(updatedClass){
    // console.log(updatedClass)
    updatedClass.studentList = this.extractStudentIds(updatedClass.studentList);
    this.httpClient.post(`${this.serverUrl}/updateStudentClassList`,updatedClass,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      // console.log(result);
    }).catch((err) => {
      // console.log('error has ocuured while updating students class list',err);
    });
  }

  saveUpdatedClass(updatedClass){
    let clonedUpdatedClass = new Class().clone(updatedClass);
    updatedClass.studentList = this.extractStudentIds(updatedClass.studentList);
    this.httpClient.post(`${this.serverUrl}/updateClass`,updatedClass,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      this.setSelectedClass(clonedUpdatedClass);
      this.successMessage('class has been updated');
      this.switchComponentByUrl('/schedule')
    }).catch((err) => {
      // console.log('failed to update classs',err);
      this.errorMessage('failed to update the class');
    });
  }

  async getStudentsById(studentIdList){
    // console.log('getting studetns by ID',studentIdList)
    let classStudentList = [];
    let updatedStudentList = [];
    studentIdList.forEach(element => {
      let checkStudentExists = this.getStudentFromArray(element);
      if(!checkStudentExists){
        updatedStudentList.push(element);
      }
      else{
        classStudentList.push(checkStudentExists);
      }
    });
    // console.log('sending this info to server',updatedStudentList);
    updatedStudentList.length > 0 && await this.httpClient.post(`${this.serverUrl}/getStudentsById`,{'list' : updatedStudentList},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then(async(result : any) => {
      result =  this.cloneStudentList(result.studentList);
      if(result[0].enrolledClasses){
        for(let student of result){
          // console.log('iterating',student)
          student.enrolledClasses = await this.getClassesById(student.enrolledClasses);
        }

      }
      classStudentList = classStudentList.concat(result);
      this.studentList = this.studentList.concat(result);
      // console.log(classStudentList)
    }).catch((err) => {
      // console.log('failed to get students by id',err);
    });
    // console.log('returned students are ',classStudentList)
    return classStudentList;
  }

  async getStudentsByIdForClass(studentIdList){
    let classStudentList = [];
    let updatedStudentList = [];
    studentIdList.forEach(element => {
      let checkStudentExists = this.studentListForClass.find(student => element == student.id)
      if(!checkStudentExists){
        updatedStudentList.push(element);
      }
      else{
        classStudentList.push(checkStudentExists);
      }
    });
    // console.log('sending this info to server',updatedStudentList);
    updatedStudentList.length > 0 && await this.httpClient.post(`${this.serverUrl}/getStudentsByIdForClass`,{'list' : updatedStudentList},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result : any) => {
      classStudentList = classStudentList.concat(result);
      this.studentListForClass  = this.studentListForClass.concat(result);
      // console.log(classStudentList,'only for class')
    }).catch((err) => {
      // console.log('failed to get students by id',err);
    });
    // console.log('returned students are ',classStudentList)
    return classStudentList;
  }

  async getClassesById(classIdList){
    let newClasses = [];
    let studentClassList = [];
    let updatedClassIdList = [];
    for(let element of classIdList) {
      let classExits = this.getClassFromArray(element);
      if(!classExits){
        updatedClassIdList.push(element);
      }
      else{
        studentClassList.push(classExits)
      }
    }
    updatedClassIdList.length <= 0 ? newClasses = studentClassList : await this.httpClient.post(`${this.serverUrl}/getClassesById`,{'list' : updatedClassIdList},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then( async(result : any) => {
      newClasses = result.map( c => new Class().clone(c))
      for (const c of newClasses) {
        if(c && c.studentList[0] && !(c.studentList[0].fName)){
          c.studentList = await this.getStudentsByIdForClass(c.studentList);
        }
        // console.log({newClasses});
        studentClassList = studentClassList.concat(c.studentList);
      }
      // console.log(newClasses)  
      this.classes = this.classes.concat(newClasses); 
      
    }).catch((err) => {
      // console.log('failed to get classes by id',err);
    });
    return newClasses;
  }

  async getClasses(){
    $('.spinner').show();
    await this.httpClient.request(new HttpRequest('GET',`${this.serverUrl}/getClasses`,{
      headers : this.getHeaders()
    }))
    .toPromise()
    .then(async (result : any) => {
      let newClasses = [];
      await result.body.classes.forEach( c => {
        let classExits = this.getClassFromArray(c._id);
        if(!classExits){
          newClasses.push(c);
        }
      })
      newClasses.forEach(async (c) => {
        c.studentList = await this.getStudentsByIdForClass(c.studentList);
        this.classes.push(new Class().clone(c));
      });
    }).catch((err) => {
      // console.log('failed to getClasses');
    });
    $('.spinner').hide(1000);
  }

  extractStudentIds(studentList){
    return studentList.map((student) => {
      return student.id;
    })
  }

  deleteClass(c){
    this.httpClient.post(`${this.serverUrl}/deleteClass`,{id :c.id},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result : any) => {
      this.classes.splice(this.classes.findIndex(element => element.id == c.id),1);
      this.successMessage(c.name + ' class has been deleted');
      this.switchComponentByUrl('/schedule');
    }).catch((err) => {
      this.errorMessage('Failed to delete ' + c.name  + ' class')
      // console.log('failed to delete class from server');
    });
  }

  postVideoLink(video){
    let vid = {
      data : video
    }
    this.httpClient.post(`${this.serverUrl}/saveVideo`,vid,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      this.successMessage('video has been successfully uploaded')
      this.videos.push(result);
    }).catch((err) => {
      this.errorMessage('failed to upload the video');
    });
  }

  postGalleryImage(image){
    let img = {
      data : image
    }
    this.httpClient.post(`${this.serverUrl}/saveImage`,img,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result : any) => {
      this.successMessage('image has been sucessfully uploaded');
      this.images.push(result.image);
    }).catch((err) => {
      this.errorMessage('Failed to upload the image')
      // console.log('error in posting image to server',err);
    });
  }

  async getGallery(){
    $('.spinner').show();
    await this.httpClient.request(new HttpRequest("GET",`${this.serverUrl}/getGallery`))
    .toPromise()
    .catch((reason)  => {
      // console.log('error occured in getting images from server',reason);
    })
    .then((result : any) => {
      // console.log('getting gallery',result);
      this.images = result.body.images;
      this.videos = result.body.videos;
    })
    $('.spinner').hide(1000);
  }

  setProfile(person){
    this.profile = person;
  }

  setSelectedClass(selectedClass){
    this.selectedClass = selectedClass;
    //this.session.store('selectedClass',JSON.stringify(selectedClass));
    this.switchComponentByUrl('/class');
  }

  async getProfile(){
    $('.spinner').show();
    if(!this.profile && this.loggedUserId){
      let p = this.studentList.find(student => student.id == this.loggedUserId) || (await this.getStudentsById([this.loggedUserId]))[0];
      this.setProfile(p);
    }
    $('.spinner').hide(1000);
  }

  deleteStudent(student){
    this.httpClient.post(`${this.serverUrl}/deleteStudent`,student,{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((response : any) => {
      this.studentList = this.cloneStudentList(response.students);
      this.successMessage(student.fName + 'has been removed from the Academy');
      this.switchComponentByUrl('/students');
    }).catch((err) => {
      this.errorMessage('Failed to delete ' + student.fName + ' from the Academy')
      // console.log("can't delete studengt",err);
    });
  }

  saveNotification(notification,studentId = 'null'){
    this.httpClient.post(`${this.serverUrl}/saveNotification`,{notification,studentId},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((result) => {
      this.successMessage('your request has been sent');
      // console.log(result)
    }).catch((err) => {
      this.errorMessage('failed to send your request');
    });
  }


  getNotifications(collectionName){
    this.httpClient.post(`${this.serverUrl}/getNotifications`,{collectionName})
    .toPromise()
    .then((result : any) => {
      this.notifications = result;
      for(let i = 10; i > 0; i--){
        this.notifications.push(result[0])
      }
    }).catch((err) => {
      // console.log(err);
    });
  }
  
  deletePicture(image){
    this.httpClient.post(`${this.serverUrl}/deletePicture`,{id : image._id},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((response : any) => {
      // console.log(response);
      this.successMessage('Picture has been deleted');
      this.images.splice(this.images.findIndex((element) => {
        return element._id == image._id;
      }),1);
    }).catch((err) => {
      // console.log('deleting image failed from server');
      this.errorMessage('Failed to delete the picture')
    });
  }

  deleteVideo(video){
    this.httpClient.post(`${this.serverUrl}/deleteVideo`,{id : video._id},{
      headers : this.getHeaders()
    })
    .toPromise()
    .then((response : any) => {
      // console.log(response);
      this.successMessage('video link has been deleted');
      this.videos.splice(this.videos.findIndex((element) => {
        return element._id == video._id;
      }),1);
    }).catch((err) => {
      this.errorMessage('Failed to delete the video link')
    });
  }

  deleteAll(){
    this.httpClient.request(new HttpRequest("GET",`${this.serverUrl}/deleteAll`))
    .toPromise()
    .then((response : any) => {
      this.studentList = this.cloneStudentList(response.students);
      this.successMessage('everything is cleaned up!');
      this.switchComponentByUrl('/students');
    }).catch((err) => {
      // console.log("can't delete studengt",err);
    });
  }

  cloneStudentList(list){
    let newList = list.map((student) => {
      return new Student().clone(student);
    });
    return newList;
  }
}
