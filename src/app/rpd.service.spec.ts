import { TestBed } from '@angular/core/testing';

import { RpdService } from './rpd.service';

describe('RpdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RpdService = TestBed.get(RpdService);
    expect(service).toBeTruthy();
  });
});
