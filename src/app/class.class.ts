import { NgbDate } from "@ng-bootstrap/ng-bootstrap";
import { Student } from "./student.class";

export class Class {
    public studentList : Student[] = [];
    public additionalInfo : string = ''; 
    public time = [];
    public date : NgbDate[] = [];

    constructor(public name:string = 'dummy',public instructor:string = 'Kulvir Thakhar',public maxStudent:number = 20,
        public id : string = 'null') {
    }

    addStudent(student) : boolean{
        if(!(student instanceof Student) || this.studentList.length >= this.maxStudent || this.studentList.find(element => 
            element.fName == student.fName && element.lName == student.lName && element.email == student.email || student.id == element.id))
        {
            return false;
        }
        else{
            this.studentList.push(student);
            return true;
        }
    }

    setTiming(date : NgbDate[],time : any[]){
        this.time = time;
        this.date = date;
    }

    changeAdditionalInfo(info:string){
        this.additionalInfo = info;
    }

    clone(origianlClass){
        this.name =  origianlClass.name;
        this.instructor = origianlClass.instructor;
        this.maxStudent = origianlClass.maxStudent;
        this.id = origianlClass._id || origianlClass.id;
        this.date = origianlClass.date.map((element) => {
          return new NgbDate(Number(element.year),Number(element.month),Number(element.day));
        })
        this.time = origianlClass.time.map((element) => {
          return {hour : Number(element.hour), minute : Number(element.minute)};
        });
        this.studentList = origianlClass.studentList;
        //this.studentList = origianlClass.studentList.map((student) => {
        //    return new Student().clone(student);
        //})
        return this;
    }
    
}
