import { Component, OnInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import swal from 'sweetalert2'
import { Class } from '../class.class';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  pageName = 'schedule'
  askDayToSchedule : boolean = false;
  error = null;
  showClasses : boolean = false;
  scheduledClasses = [];
  askTime : boolean = false;
  date : NgbDate[] = [];
  time  = [];
  timeModal;
  newClass : Class = null;
  
  constructor(private service : RpdService) { 
  }

  showTime(){
    //this.changeTime = true; changeDate = false;
  }

  async toggleShowClasses(){
    this.showClasses = !this.showClasses;
  }

  selectDate(){      
    if (this.date.length <= 0){
      this.error = 'invalid date! please enter a valid date';
    }
    else {
      this.askTime = true;
      this.error = null;
      this.askDayToSchedule = false;
      this.showClasses = false;
    }
  }

  getClassName(){
    return swal({
      title: 'What\'s the class name',
      allowOutsideClick: false,
      type : 'question',
      input : 'text',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputPlaceholder: 'class name',
      inputValidator : (value) => {
        return !value &&  'Error! Please enter a valid name';
      },  
      preConfirm : (value) => {        
        return new Promise((resolve,reject) => {          
          this.newClass.name = value;
          return this.getInstructorName();
        })
      }   
    })
  }

  getInstructorName(){
    return swal({
      title: 'What\'s the instructor name',
      allowOutsideClick: false,
      type : 'question',
      input : 'text',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputPlaceholder: 'instructor name',
      inputValidator : (value) => {
        return !value &&  'Error! Please enter a valid name';        
      },
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {
            this.newClass.instructor = value;
            return this.getClassSize();
        })
      }   
    })
  }
  
  getClassSize(){
    return swal({
      title: 'What\'s the class size',
      allowOutsideClick: false,
      type : 'question',
      input : 'number',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputPlaceholder: 'class size',
      inputValidator : (value) => {
        return !value && 'Error! Please enter a valid size';                
      },
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {          
          this.newClass.maxStudent = value;
          return this.getClassInfo();
        })
      }   
    })
  }

  addDate(event){
    this.date.push(event); 
  }

  showDateInfo(date){
    console.log(date)
    this.scheduledClasses = [];
    this.service.profile && this.service.profile.enrolledClasses.forEach(c => {
      if(c.date.findIndex(element => element.equals(date)) != -1){
        this.scheduledClasses.push(c);
      }
    });
  }

  hasClassPassed(date){
    if(this.service.profile && this.service.profile.enrolledClasses.findIndex(c => {
      return c.date.findIndex(element => element.equals(date)) != -1;
    }) != -1){
      let today = new Date();
      return date.before(new NgbDate(today.getFullYear(),today.getMonth(),today.getDay()));
    }
  }

  isClassScheduled(date){
    return this.service.profile && this.service.profile.enrolledClasses.findIndex(c => {
      return c.date.findIndex(element => element.equals(date)) != -1;
    }) != -1;
  }

  isDateSelected(date) {
    if(this.date.findIndex(element => element.equals(date)) != -1 ){
      return true;
    }
    return false;
  }

  getClassInfo(){
    return swal({
      title: 'Any additional information',
      allowOutsideClick: false,
      type : 'question',
      input : 'textarea',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputPlaceholder: 'class name', 
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {
          this.newClass.additionalInfo = value;
          this.service.createClass(this.newClass);
          resolve();
        })
      }   
    })
  }

  selectClass(c){
    console.log('selected class is ',c)
    this.service.setSelectedClass(c);
  }

  createClass(){
    this.showClasses = false;
    this.askDayToSchedule = true;
    this.newClass = new Class();
  }

  selectTime(){
    if (!this.timeModal) {
      this.error = 'invalid time! please enter a valid time';
      return;
    }

    this.time.push(this.timeModal);

    if(this.date.length == this.time.length) {
      console.log(this.date,this.time);
      this.error = null;
      this.askTime = false;
      this.showClasses = true;
      this.askDayToSchedule = false;
      this.newClass.setTiming(this.date,this.time);
      this.getClassName()
    }
  }

  ngOnInit() {
    this.service.getClasses();
  }

}
