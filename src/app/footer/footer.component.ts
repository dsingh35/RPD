import { Component, OnInit } from '@angular/core';
import * as $ from "jquery-easing";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready($('.g-ytsubscribe iframe').css('margin-top','-3px'));
  }

}
