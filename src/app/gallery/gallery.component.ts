import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import swal from 'sweetalert2'
import * as $ from "jquery-easing";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})

export class GalleryComponent implements OnInit,AfterViewInit {

  constructor(private service : RpdService) { }

  private pageName = 'gallery';
  private picIsHidden = "false";
  private vidIsHidden = "true";
  modelImgId = 0;
  private newImg;
  showDeleteButton = false;
  private carousel = false;

  ngOnInit() {
    this.service.getGallery();
  }

  ngAfterViewInit(){
    $('.animation').delay(1000).show(1000);
  }

  saveImg(img){
    this.service.postGalleryImage(img);
  }

  scrollPage(){
    window.scrollBy(0,650)
  }

  prevImg(){
    this.modelImgId--;
    this.showImage(this.modelImgId);
  }

  nextImg(){
    this.modelImgId++;
    this.showImage(this.modelImgId);
  }

  showImage(imgId){
    var modalImg = $("#modelImg");
    this.modelImgId = (this.service.images.length + imgId) % this.service.images.length;
    let img = this.service.images[this.modelImgId].data;
    $('.bgImage').removeClass('fixed-top')
    modalImg.attr('src',img);
    var modal = $('#imgModel');
    modal.show(1000);
    $('.close').on('click',() => {
      modal.hide(1000);
      $('.bgImage').addClass('fixed-top')
    })
  }

  saveVideo(vid){
    this.service.postVideoLink(vid);
  }

 async getImage(){
    swal({
      title: 'upload a new picture',
      input: 'file',
      allowOutsideClick: false,
      inputAttributes: {
        'accept': 'image/*',
          'aria-label': 'Upload a new picture'
       },
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      preConfirm : (file) => {
        return new Promise((resolve,reject) => {
          if (file) {
            const reader = new FileReader
            reader.onload = (e) => {
              this.saveImg(e.target.result);
            }
            reader.readAsDataURL(file)
          }
          else{
            reject("file not supported");
          }
          resolve(this.newImg);
        })
      }
    })
  }

  makeEmbeddedCode(urlLink : string){
    urlLink = urlLink.replace('watch?v=','embed/');
    console.log(urlLink);
    return urlLink;
  }

  getVideoLink(){
    swal({
      title : 'upload a new link to video on youtube',
      input : 'text',
      allowOutsideClick : false,
      type : 'info',
      inputValidator : (value) => {
        return !value && 'please enter a valid embedded link'
      },
      preConfirm : (value) => {
        this.saveVideo(this.makeEmbeddedCode(value));
      }
    })
  }

}

