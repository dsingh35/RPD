import { Component, OnInit, Input } from '@angular/core';
import * as $ from "jquery-easing";
import { RpdService } from '../rpd.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  constructor(private service : RpdService) { }

  showOptions(div){
    $(div).slideToggle(1000,'easeOutExpo');
  }

  ngOnInit() {
    $('.options').slideUp();
  }

}
