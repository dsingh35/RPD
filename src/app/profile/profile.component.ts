import { Component , OnInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import { FormGroup , FormControl , Validators } from "@angular/forms";
import { Student } from '../student.class';
import swal from 'sweetalert2';
import * as $ from 'jquery';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  private profile = null;
  private fg = null;
  private pageName = 'profile';
  private edit = false;
  feedbacks = [{giver: 'kulvir' ,fb:"you are not very good at it",date : {month : 11, day:15, year : 2018} },{giver:'kulvir' ,fb:"you suck",date : {month : 11, day:15, year : 2018} },{giver:'kulvir' ,fb:"you are awesome",date : {month : 11, day:15, year : 2018} }];
  constructor(private service : RpdService) { 
  }
  
  toggleEdit(){
    this.edit = !this.edit;
  }

  deleteStudent(){
    this.service.deleteStudent(this.service.profile);
  }
  
  showImage(){
    $('.bgImage').removeClass('fixed-top')
    $('#imgModel').css('display','block');
    $('.close').on("click",() => {
      $('#imgModel').css('display','none');
      $('.bgImage').addClass('fixed-top')
    })
  }

  getMessageFromUser(){
    swal({
      title : 'what\'s the message?',
      type : 'question',
      input : 'textarea',
      showCancelButton : true,
      showLoaderOnConfirm : true,
      inputValidator : (value) => {
        return !value && 'please enter a valid message';
      },
      preConfirm : (value) => {
        this.service.saveNotification(value,this.service.profile.id)
      }
    })
  }

  async onSubmit(value){
    console.log('on submit',value)
    value.address = {
      suite : value.suite,
      city : value.city,
      street : value.street,
      province : value.province,
      postal : value.postal 
    }
    let imgObject = <any>document.getElementById('image');

    if(imgObject.files.length <= 0){
      value.img = this.service.profile.img;
      value = Object.assign(this.service.profile,value)    
      this.service.editStudent(new Student().clone(value));      
    }
    else{
      const reader = new FileReader();
       reader.onload = (e) => {
        value.img = e.target.result;
        value = Object.assign(this.service.profile,value)    
        this.service.editStudent(new Student().clone(value));
      }  
      await reader.readAsDataURL(imgObject.files[0]);
    }
    
    this.toggleEdit();
  }
  
  ngOnInit() {
    if(!this.service.profile){
      this.service.switchComponentByUrl(' ');
    }
    this.fg = new FormGroup({
      fName : new FormControl(this.service.profile.fName, Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      lName : new FormControl(this.service.profile.lName,Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      age : new FormControl(this.service.profile.age,Validators.compose([Validators.required,Validators.pattern('[0-9]+')])),
      phone : new FormControl(this.service.profile.phone,Validators.compose([Validators.required,Validators.pattern('[0-9]{10}')])),
      email : new FormControl(this.service.profile.email,Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+@[a-zA-Z]+\.[a-zA-Z]+')])),
      suite : new FormControl(this.service.profile.address.suite,Validators.required),
      street : new FormControl(this.service.profile.address.street,Validators.required),
      city : new FormControl(this.service.profile.address.city,Validators.required),
      province : new FormControl(this.service.profile.address.province,Validators.required),
      postal : new FormControl(this.service.profile.address.postal,Validators.required),
      img : new FormControl(null)
    });
  }

}
