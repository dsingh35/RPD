import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { trigger, state, transition, style, animate } from "@angular/animations";
 import * as $ from "jquery-easing";
import { RpdService } from '../rpd.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  pageName = "home";
  logo = true;
  importanceIndex = Math.round(Math.random()*4);
  importance : string[] = ['Creative thinking skills are developed through dance, as well as learning the value of discipline,commitment and work ethic. Self-confidence develops as young people overcome challenges to master new goals,learning to apply themselves and accomplish any task put before them.',
                          'Dance teaches children about music, rhythm and beat. Students also have a better understanding of spatial relationships and learn to think with both sides of their brain. All these skills enhance a child’s academic performance, as well as their physical well-being.',
                          'Dance keeps you fit! Dance teaches the importance of movement and fitness in a variety of ways through a variety of disciplines. As well, dancers learn to coordinate muscles to move through proper positions. Dancing is a great activity to pursue at almost any age provided you are in proper health to handle the rigors of dancing for life.',
                          ' Every child benefits from quality dance training. Long-term learnings go far beyond practical applications in dance. Dancers enter society with the ability to maintain the uncompromising high standards nurtured during their dance training.',
                          ' Dancers learn to take turns, to share attention, and to cooperate with others as they work within a group. These life lessons are part of the appeal of dance classes to parents around the world.  Much like team sports, dance for children can teach some invaluable and important lessons.'];
  constructor(public service : RpdService) {
   }

  ngOnInit() {
    this.service.getProfile();
    this.service.isAdmin();
  }

  ngAfterViewInit(){
    $('.intro a').delay(1500).hide();
    $('.intro a').delay(1000).show(500,()=>{
      $('.intro img').removeClass('enlarge-animation').addClass('delarge-animation');
      $('.intro a').delay(500).animate({
        'marginTop' : -100
      },1000,()=>{
        this.logo = false;
      })
    });

    setInterval(()=> {
      $('.importance').slideUp(500,()=>{
        this.importanceIndex = Math.floor(Math.random()*4);
      });
      $('.importance').slideDown(500,()=>{
        $('.importance').stop();
      });
    },5000)
    
    $('.textAnimation span').delay(1500).each( function (index) {
      $( this ).delay((Number(index) * 200)).animate({
        left : (23 + index*8) + '%',
        opacity : 1
        },1000,'easeOutCirc',()=>{
        })
    })

    $('.textAnimation span').delay(1000).each(function(){
      $( this ).delay(1000).hide(500)
    })

    $('.animation span').delay(6500).each( function (index) {
      $( this ).delay((Number(index) * 300)).animate({
        left : (35 + index*11) + '%',
        },0,() => {
          $( this ).animate({
            top : '55vh',
            opacity : 1
          },1500,'easeOutBounce')})
    })
    
    $('.animation a').delay(6000).toggle(1000)

  }

}
