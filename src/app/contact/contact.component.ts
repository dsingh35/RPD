import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from "jquery-easing";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit,AfterViewInit {

  constructor(public service : RpdService) { }
  loading = false
  private pageName = 'contact'
  private fg = null;
  collectionName = 'contactRequest';
  async onSubmit(value){
    $('.spinner').show(500);
    value.address = {
      suite : value.suite,
      street : value.street,
      city : value.city,
      province : value.province,
      postal : value.postal
    }
    delete value.suite; delete value.street; delete value.city; delete value.province;delete value.postal;
    console.log(value);
    await this.service.saveNotification(value);
    this.loading = false;
    $('.spinner').show(500);
  }

  showNotifications(){
    this.service.switchComponentByUrl('/notifications')
  }

  scrollPage(){
    window.scrollBy(0,600)
  }

  isHidden(el) {
    console.log(el)
    return (el.offsetParent === null)
  }

  changeHeight(){
    if((<any>document.getElementsByClassName('navbar-toggler')[0]).offsetParent === null){
      $('.bgImage').css('height','100vh');
    }else{
      $('.bgImage').css('height','');
    }
  }

  ngOnInit() {
    this.fg = new FormGroup({
      fName : new FormControl('', Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      lName : new FormControl('',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      age : new FormControl('',Validators.compose([Validators.required,Validators.pattern('[0-9]+')])),
      phone : new FormControl('',Validators.compose([Validators.required,Validators.pattern('[0-9]{10}')])),
      email : new FormControl('',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+@[a-zA-Z]+\.[a-zA-Z]+')])),
      suite : new FormControl('',Validators.required),
      street : new FormControl('',Validators.required),
      city : new FormControl('',Validators.required),
      province : new FormControl('',Validators.required),
      postal : new FormControl('',Validators.required),
      details : new FormControl('',Validators.required),
      reason : new FormControl('', Validators.required)
    });
    $('.spinner').hide(500);
  }

  ngAfterViewInit(){
    $('.textAnimation span').delay(1000).each( function (index) {
      let top = '20vh';
      let delay = 100;
      if(index >= 14){
        top = '40vh';
        index = index-12;
      }
      $( this ).delay(delay*index).animate({
        left : (2 + index*4) + '%',
        },0,() => {
          $( this ).animate({
            'top' : top,
            opacity : 1
          },1500,'easeOutBounce')})
    })

    $('.textAnimation img').delay(4000).show(1000);
  }
}
