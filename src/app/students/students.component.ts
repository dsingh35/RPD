import { Component , OnInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import swal from 'sweetalert2'
import { Student } from '../student.class';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  public service = null;
  public addNewStudent : boolean = false;
  public newStudent = null;
  public pageName = "students";

  constructor(service: RpdService) {
    this.service = service;
  }

  toggleAddNewStudent(){
    this.addNewStudent = !this.addNewStudent;
  }

  async showProfile(person){
    this.service.profile = person;
    this.service.switchComponentByUrl('/students/profile');
  }

  getImage(){
    swal({
      title: 'upload student\'s image',
      input: 'file',
      allowOutsideClick: false,
      inputAttributes: {
        'accept': 'image/*',
        'aria-label': 'Upload the profile picture'
       },
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      preConfirm : (file) => {
        return new Promise((resolve,reject) => {
          console.log(file)
          if (file) {
            const reader = new FileReader
            reader.onload = (e) => {
              this.newStudent.img = e.target.result;
              this.getName();
            }
            reader.readAsDataURL(file)
          }
          else{
            reject("file not supported");
          }
        })
        }
      })
  }

  getName(){
    swal({
      title: 'Enter Student\'s name',
      imageUrl: this.newStudent.img,
      allowOutsideClick: false,
      imageAlt: 'The uploaded picture',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputPlaceholder: 'kulvir',
      imageWidth: 250,
      imageHeight: 200,
      html: '<input id="first" placeholder="First Name" class="swal2-input">'+
            '<input id="last" placeholder="Last Name" class="swal2-input">',
      preConfirm : () => {
        return new Promise((resolve,reject) => {
          let first = (<HTMLInputElement>document.getElementById('first')).value;
          let last = (<HTMLInputElement>document.getElementById('last')).value;
          if(!first || !last)
            swal.showValidationError("can't be empty")
          else{
            this.newStudent.fName = first;
            this.newStudent.lName = last;
            console.log(first)
            this.getAge();
          }
        })
      }   
    }) 
  }

  getAge(){
    swal({
      title: `Enter ${this.newStudent.fName} ${this.newStudent.lName}\'s Age`,
      input: 'range',
      imageUrl: this.newStudent.img,
      allowOutsideClick: false,
      imageAlt: 'The uploaded picture',
      imageWidth: 250,
      imageHeight: 200,
      inputPlaceholder: 'kulvir',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      inputAttributes : {
        min: '1',
        max: '100',
        step: '1'
      },
      inputValue : '20',
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {
          if(!value)
            swal.showValidationError("can't be empty")
          else{
            this.newStudent.age = value;
            this.getEmail();
          }
        })
      }   
    })
  }
  
  getEmail(){
    swal({
      title: `Enter ${this.newStudent.fName} ${this.newStudent.lName}\'s email`,
      imageUrl: this.newStudent.img,
      allowOutsideClick: false,
      imageAlt: 'The uploaded picture',
      imageWidth: 250,
      imageHeight: 200,
      input: 'email',
      inputPlaceholder: 'somebody@example.com',
      showCancelButton: true,
      confirmButtonText: 'Next',
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm : true,
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {
          
          this.newStudent.email = value;
          this.getPhone();
        })
      }
    })
  }
  
  getPhone(){
    swal({
      title: 'Enter Student\'s phone number',
      input: 'text',
      inputPlaceholder: '0123456789',
      imageUrl: this.newStudent.img,
      allowOutsideClick: false,
      imageAlt: 'The uploaded picture',
      imageWidth: 250,
      imageHeight: 200,
      confirmButtonText : 'next',
      showCancelButton: true,
      inputValidator: (value) => {
        return (!value || !(/^[0-9]{10}$/.test(value))) && 'please enter a valid phone number';
      },
      preConfirm : (value) => {
        return new Promise((resolve,reject) => {        
          if(!value){
            swal.showValidationError("can't be empty")
          }
          else if(value.trim().length != 10  || isNaN(value.trim())){
            swal.showValidationError("number should be 10 digits long")
          }
          else{
            this.newStudent.phone = Number(value);
            this.getAddress();
          }
        })
      }
    })    
  }

    getAddress(){
      swal({
        allowOutsideClick: false,
        confirmButtonText: 'Create',
        showCancelButton : true,
        showLoaderOnConfirm : true,
        cancelButtonText : 'Cancel',
        title: 'Enter Student\'s address',
        html: '<input id="suite" placeholder="suite" class="swal2-input">' +
              '<input id="street" placeholder="street" class="swal2-input">' +
              '<input id="city" placeholder="city" class="swal2-input">' +
              '<input id="province" placeholder="province" class="swal2-input">' +
              '<input id="postal" placeholder="postal" class="swal2-input">',
        preConfirm : () => {
          return new Promise((resolve,reject) => {
            let address = {
              suite : (<HTMLInputElement>document.getElementById('suite')).value,
              street : (<HTMLInputElement>document.getElementById('street')).value,
              city : (<HTMLInputElement>document.getElementById('city')).value,
              province : (<HTMLInputElement>document.getElementById('province')).value,
              postal : (<HTMLInputElement>document.getElementById('postal')).value
            }
            
            if(!address.suite || !address.city || !address.province || !address.postal || !address.street)
              swal.showValidationError("can't be empty")
            else{
              this.newStudent.setAddress(address);
              console.log(this.newStudent);
              this.service.successMessage();
              resolve("done");
              this.service.addStudent(this.newStudent);
              this.newStudent = null;
            }
          })
        }
      })
  }

  async createStudent(){
    this.newStudent = new Student();
    this.getImage();
  }

  ngOnInit() {
  }

}
