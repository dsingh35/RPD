export class Student{
    public enrolledClasses = [];
    public peerRating = [];
    public instructorRating = [];
    public peerRatingAvg = 0;
    public notifications = [];
    public instructorRatingAvg = 0;
    public role = 'Student';
    public feedBacks = [];

    constructor(public fName : string = "dummy",public lName : string = "dummy",public img : any = "dummy", public email : string = "dummy", public phone : number = -1,
                public age : number = -1, public address : any = null,public id : string = 'null'){
        if(address != null){
            this.setAddress(address);
        }
    }

    setAddress(add:any){
        this.address = new Address(add.suite,add.street,add.city,add.province,add.postal);
    }

    rate(rating,person : Student){
        if(person.role == 'Student'){
            this.peerRating.push(rating);
            this.peerRatingAvg = this.calculateRatingAvg(this.peerRating);
        }
        else{
            this.instructorRating.push(rating);
            this.instructorRatingAvg = this.calculateRatingAvg(this.instructorRating);
        }
    }

    giveFeedback(feedback){
        if(this.feedBacks.length >= 3){
            this.feedBacks.shift();
        }
        this.feedBacks.push(feedback);
    }

    calculateRatingAvg(ratingType){
        return ratingType.reduce((a,b) => a+b)/ratingType.length;
    }

    enrollClass(classID){
        if(!this.enrolledClasses.includes(classID)){
            this.enrolledClasses.push(classID);
            return true;
        }
        return false;
    }

    withdrawClass(classID){
        let index = this.enrolledClasses.findIndex( id => id == classID);
        if(index != -1){
            this.enrolledClasses.splice(index,1);
            return true;
        }
        return false;
    }

    clone(originalStudent){
        this.fName = originalStudent.fName;
        this.lName = originalStudent.lName;
        this.enrolledClasses = originalStudent.enrolledClasses;
        this.peerRating = originalStudent.peerRating;
        this.instructorRating = originalStudent.instructorRating;
        this.role = originalStudent.role;
        this.feedBacks = originalStudent.feedBacks;
        this.img = originalStudent.img;
        this.email = originalStudent.email;
        this.notifications = originalStudent.notifications;
        this.phone = originalStudent.phone;
        this.age = originalStudent.age;
        this.setAddress(originalStudent.address);
        this.id = originalStudent._id || originalStudent.id;
        return this;
    }
}

class Address { 
    constructor(public suite,public street, public city, public province, public postal){}
}