import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { StudentsComponent } from './students/students.component';
import { RpdService } from './rpd.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { RouterModule,Routes } from "@angular/router";
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { Ng2Webstorage } from "ngx-webstorage";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContactComponent } from './contact/contact.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClassComponent } from './class/class.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SafePipe } from './urlSanitizer.class';
import { NotificationsComponent } from './notifications/notifications.component';
import { FooterComponent } from './footer/footer.component';
import { ServicesComponent } from './services/services.component';
import { MatProgressSpinnerModule,MatInputModule, MatSelectModule,MatFormFieldModule } from '@angular/material';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [
    AppComponent,
    SafePipe,
    HeaderComponent,
    StudentsComponent,
    HomeComponent,
    FooterComponent,
    ProfileComponent,
    ContactComponent,
    GalleryComponent,
    ScheduleComponent,
    ClassComponent,
    ResetpasswordComponent,
    NotificationsComponent,
    FooterComponent,
    ServicesComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger'
    }),
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    Ng2Webstorage,
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    RouterModule.forRoot([
      { 
        path : 'students', component : StudentsComponent 
      },
      {
        path : 'students/profile', component : ProfileComponent
      },
      {
        path : 'contact', component : ContactComponent
      },
      {
        path : 'gallery', component : GalleryComponent
      },
      {
        path : 'schedule', component : ScheduleComponent
      },
      {
        path : 'class', component : ClassComponent
      },
      {
        path : 'resetPassword/:token', component : ResetpasswordComponent
      },
      {
        path : 'notifications', component : NotificationsComponent
      },
      {
        path : '**', component : HomeComponent, redirectTo : ''
      }
    ])
  ],
  providers: [RpdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
