import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RpdService } from '../rpd.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  jwt: string = '';
  user = null;

  constructor(public router : ActivatedRoute, public service : RpdService) { }

  async ngOnInit() {
    this.jwt = this.router.snapshot.params['token'];
    let verifiedEmail = (await this.service.verifyToken(this.jwt));
    if(verifiedEmail){
      this.user = await this.service.checkStudentExist(verifiedEmail.email);
      this.getNewPassword();
    }
    else{
      this.service.errorMessage('link has expired. Please start over again.')
      this.service.switchComponentByUrl(' ');
    }
  }

  getNewPassword(){
    swal({
      title : `${this.user.fName}, Please create a new Password`,
      html : '<input id="pass" type="text" class="swal2-input" placeholder="new password">'+
             '<input id="cPass" type="text" class="swal2-input" placeholder="new password">',
      imageUrl : this.user.img,
      imageWidth : 150,
      showCancelButton : true,
      preConfirm : () => {
        return new Promise( async (resolve) => {
          let pass = (<HTMLInputElement>document.getElementById('pass')).value;
          let cPass = (<HTMLInputElement>document.getElementById('cPass')).value;
          if(!pass.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/)){
            this.service.errorMessage('password should be atleast of 6 letters and should contain numbers and special characters')
            document.location.reload();
          }
          else if(pass !== cPass){
            this.service.errorMessage('password doesn\'t match');
            document.location.reload();            
          }
          else{
            await this.service.resetPassword(this.jwt,this.user.email,pass);
          }
        })
      }
    })
  }


}
