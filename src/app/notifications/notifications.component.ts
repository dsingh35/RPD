import { Component, OnInit } from '@angular/core';
import { RpdService } from '../rpd.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  notificationDetails = false;
  notificationToShow = null;
  showSenderInfo = false;
  pageName = 'notifications';
  
  constructor(private service : RpdService) {
   }

  ngOnInit() {
    this.service.getProfile();
  }

  deleteNotifiacation(){
    this.notificationToShow && this.service.deleteNotification(this.notificationToShow._id,'contactRequest')
    this.notificationToShow = null;
  }

  showNotification(notification){
    this.notificationToShow = notification;
    console.log(notification,this.notificationToShow)
    this.notificationDetails = true;
    if(notification.status == 'unread'){
      this.service.markNotificationRead(this.notificationToShow._id);
      this.notificationToShow.status = 'read';      
    }
  }

}
