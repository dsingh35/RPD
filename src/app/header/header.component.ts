import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import * as $ from 'jquery-easing';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit,AfterViewInit {
  @Input() pageName : string;
  @Input() logo : boolean;
  service = null;
  newNotifications = 0;
  constructor(service: RpdService) {
    this.service = service;
  }
  
  isHidden(el) {
    return (el.offsetParent === null)
  }

  toggleProfileLgButton(){
    if($('#dropdown-lg:visible').length <= 0){ 
      $('.bgImage').animate({
         'min-height' : 300,
      })
      $('#dropdown-lg').slideDown(1000,'easeOutExpo');
    }
    else{
      $('#dropdown-lg').slideUp(1000,'easeOutExpo');
      $('.bgImage').animate({
        'min-height' : 0,
      },1000)
    }
  }

  toggleProfileSmButton(){
    if($('#dropdown:visible').length <= 0){ 
      $('.bgImage').animate({
         'min-height' : 300,
      })
      $('#dropdown').slideDown(1000,'easeOutExpo');
    }
    else{
      $('#dropdown').slideUp(1000,'easeOutExpo');
      $('.bgImage').animate({
        'min-height' : 0,
      },1000)
    }
  }

  ngOnInit() {
    document.getElementById(this.pageName) && document.getElementById(this.pageName).classList.add('active');
    this.service.profile && this.service.profile.notifications.forEach(element => {
        if(element.status == 'unread'){
          this.newNotifications++;
        }
    });
    $(document).ready(() => {
      if(this.newNotifications > 0){
        console.log('executed')
        $('.login').css('margin-right','0vw');
      }
    })
  }

  showSchedule(){
    this.service.switchComponentByUrl('/schedule');
  }

  ngAfterViewInit(){
     $('#dropdown-lg,#dropdown').slideUp();
  
     $(document).one('scroll',() => {
        if($('.navbar:visible').length > 0){
          if(document.body.scrollTop > 10 || document.documentElement.scrollTop > 10){
            $('.navbar img').css('width','50').css('position','fixed')
          }
          else {
            $('.navbar img').css('width','80');
        }
      }
    })
  }

   toggleNav(){
    if(!this.isHidden(document.getElementsByClassName('collapse')[0])){
      $('#collapsibleNavbar').hide(500)
      $('.animation').show(500);
    }else{
      $('.animation').hide(500);
      $('#collapsibleNavbar').show(500)
    }
  }

}
