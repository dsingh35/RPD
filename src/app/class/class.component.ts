import { Component, OnInit } from '@angular/core';
import { RpdService } from '../rpd.service';
import {Observable} from 'rxjs';
import {debounceTime, map} from 'rxjs/operators';
import { Class } from '../class.class';
import { Student } from '../student.class';
import { FormGroup , FormControl , Validators } from "@angular/forms";
import * as $ from "jquery-easing";

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})

export class ClassComponent implements OnInit {
  edit = false;
  pageName = 'schedule';
  selectedStudent : Student = null;
  showAvailableStudents = false;
  studentToAdd:Student = null;
  changeDate = false;
  changeTime = false;
  fg = null;
  error = null;
  tempTimeArray = [];

  constructor(public service : RpdService ) { }

  public model: any;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.service.studentList.filter(student => (student.lName+ " " + student.fName).toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: {name: string}) => x.name;
  
  async onSubmit(value){
    $('.spinner').show(500);
    this.service.selectedClass.name = value.name;
    this.service.selectedClass.instructor = value.instructor;
    this.service.selectedClass.additionalInfo = value.additionaInfo;
    this.service.selectedClass.maxStudent = value.maxStudent;
    await this.service.saveUpdatedClass(new Class().clone(this.service.selectedClass));
    await this.service.saveUpdatedStudentClassList(new Class().clone(this.service.selectedClass));
    $('.spinner').hide(500);    
  }

  askTime(){
    if(this.service.selectedClass.date.length <= 0){
      this.error = 'invalid dates! please select a valid date';
      return;
    }
    this.error = null;
    this.changeTime = true;
    this.changeDate = false;
    
  }

  selectTime(){
    if(!this.service.selectedClass.time[this.tempTimeArray.length]){
      this.error = 'invalid time! please enter a valid time';
      return;
    }
    this.error = null;
    this.tempTimeArray.push(this.service.selectedClass.time[this.tempTimeArray.length]);

    if(this.tempTimeArray.length == this.service.selectedClass.date.length){
      this.service.selectedClass.time = this.tempTimeArray;
      this.tempTimeArray = [];
      this.changeDate = false;
      this.changeTime = false;
    }

  }

  toggleDate(event){
    let index = this.service.selectedClass.date.findIndex(element => element.equals(event));
    if(index != -1){
      this.service.selectedClass.date.splice(index,1);
    }
    else{
      this.service.selectedClass.date.push(event); 
    }
  }

  isDateSelected(date) {
    if(this.service.selectedClass.date.findIndex(element => element.equals(date)) != -1 ){
        return true;
    }
    return false;
  }


  addStudent(){
    if(this.showAvailableStudents && this.studentToAdd && this.service.selectedClass){
      if(!this.service.selectedClass.addStudent(this.studentToAdd) || !this.studentToAdd.enrollClass(this.service.selectedClass.id)){
        this.service.errorMessage('student already exists')
      }
    }
    this.studentToAdd = null;
    this.showAvailableStudents = true;
  }

  removeStudent(){
    this.selectedStudent = this.selectedStudent && this.service.studentList.find(element => element.id == this.selectedStudent.id) || null;
    if(this.selectedStudent && !this.showAvailableStudents && this.service.selectedClass){
      let index = this.service.selectedClass.studentList.findIndex(element => element.id == this.selectedStudent.id);
      if(!this.selectedStudent.withdrawClass(this.service.selectedClass)){
        this.service.errorMessage('student is not enrolled in the class');
      }
      else{
        this.service.selectedClass.studentList.splice(index,1);
      }
    }
    else{
      this.showAvailableStudents = false;
      this.service.errorMessage('an error has occured');
    }
    this.selectedStudent = null;
  }

  ngOnInit() {
    if(!this.service.selectedClass){
      this.service.switchComponentByUrl('/schedule');
    }
    $('.spinner').hide(500);
    this.fg = new FormGroup({
      name : new FormControl(this.service.selectedClass.name, Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      instructor : new FormControl(this.service.selectedClass.instructor,Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]+')])),
      maxStudent : new FormControl(this.service.selectedClass.maxStudent,Validators.compose([Validators.required,Validators.pattern('[0-9]+')])),
      additionaInfo : new FormControl(this.service.selectedClass.additionalInfo,Validators.compose([Validators.nullValidator]))
    });
  }

  async deleteClass(){
    $('.spinner').show(500);
    await this.service.deleteClass(this.service.selectedClass);
    $('.spinner').hide(500);    
  }

}
