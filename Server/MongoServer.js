"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoDb = require('mongodb');
const MongoClient = MongoDb.MongoClient;

class MongoConnector{
      
  constructor(url,dbName){
    this.url = url;
    this.dbName = dbName;
    this.dbCollections = [];  
    this.connectToDB();
  }
  
  connectToDB() {
    this.db = this.db || new Promise((resolve, reject) => {
      MongoClient.connect(this.url, (err, client) => {
        if (!err) {
            resolve(client.db(this.dbName));
        }
        else {
          reject(err);
          console.log("Error connecting");
        }
      });
    });
    return this.db;
  }

  hidePassword(person){
    if(!person)
      return;
    delete person.password;
  }

  logIn(logInData){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.findOne({'email' : logInData.email,'password' : logInData.password},(err,result) => {
          if(err || result == null){
            reject(err);
          }
          else{
            this.hidePassword(result);
            resolve(result);
          }
        })
      })
    })
  }

  deleteImage(imageID){
    console.log('in mongo',imageID)
    return this.getCollection('pictures')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.deleteOne({'_id' : new MongoDb.ObjectId(imageID)},(err,result) => {
          if(err){
            console.log('in mongo, failed to delete image with image id');
            reject(err);
          }
          else{
            console.log('in mongo- image deleted')
            resolve('in mongo- done deleting image');
          }
        })
      })
    })
  }

  saveImage(img){
    return this.getCollection('pictures')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.insertOne(img,(err,result) => {
          if(err){
            console.log('in mongo, error has occured while saving picture');
            reject(err)
          }
          else{
            resolve(result.ops[0]);
          }
        })
      })
    })
  }

  saveVideo(video){
    return this.getCollection('videos')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.insertOne(video,(err,result) => {
          if(err){
            console.log('in mongo, error has occured while saving video');
            reject(err)
          }
          else{
            resolve(result.ops[0]);
          }
        })
      })
    })
  }


  deleteVideo(id){
    return this.getCollection('videos')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.deleteOne({'_id' : new MongoDb.ObjectId(id)},(err,result) => {
          if(result){
            resolve(result)
          }
          else{
            reject(err);
          }
        })
      })
    })
  }

  getNotifications(studentId){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({'_id' : new MongoDb.ObjectId(studentId)},{'notifications' : 1}).toArray((err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  markNotificationRead(studentId,notificationId){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateOne({'_id' : new MongoDb.ObjectId(studentId),'notifications._id' : new MongoDb.ObjectId(notificationId)},{$set : {'notifications.$.status' : 'read'}},(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  deleteNotification(studentId,notificationId){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateOne({'_id' : new MongoDb.ObjectId(studentId)},{$pull:{'notifications' : {'_id' : new MongoDb.ObjectId(notificationId)}}},(err,result) => {
          if(err){
            reject(err)
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  saveNotification(studentId,notification){
    notification._id = MongoDb.ObjectId();
    return this.getCollection('students')
    .then((collection)=>{
      return new Promise((resolve,reject) => {
        collection.updateOne({'_id' : new MongoDb.ObjectId(studentId)},{$push:{'notifications' : notification}},(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  cleanGallery(){
    return this.getCollection('pictures')
    .then((collection) => {
      collection.deleteMany({},(err,result)=>{
        if(err){
          console.log('failed to delete all the images');
        }
        else{
          console.log('all images are deleted successfully');
        }
      })
    })
  }

  getPictures(){
    return this.getCollection('pictures')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({}).toArray((err,result) => {
          if(err){
            console.log('error occured in mongo while getting images');
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })  
  }

  getClasses(){
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({}).toArray((err,result) => {
          if(err){
            reject(err)
          }
          else{
            resolve(result)
          }
        })
      })
    })
  }

  createClass(newClass){
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.insertOne(newClass,(err,result) => {
          if(err){
            console.log('in mongo, error occured while creating a new class');
            reject(err);
          }
          else{
            resolve(result.ops[0]);
          }
        })
      })
    })
  }

  checkEmailExists(email){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.findOne({'email' : email},(err,result) => {
          if(result){
            result = {fName : result.fName, lName : result.lName,img : result.img};
            resolve(result);
          }
          else{
            reject('no result found');
          }
        })
      })      
    })
  }

  checkIdExists(id){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.findOne({'_id' : new MongoDb.ObjectId(id)},(err,result) => {
          if(result){
            result = {fName : result.fName, lName : result.lName,img : result.img};
            resolve(result);
          }
          else{
            reject('no result found');
          }
        })
      })      
    })
  }

  getAdminId(id){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.findOne({'role' : 'RPDadmin'},(err,result) => {
          if(result){
            result = {fName : result.fName, lName : result.lName,email : result.email,id : result._id,phone : result.phone,address : result.address};
            resolve(result);
          }
          else{
            reject('no result found');
          }
        })
      })      
    })
  }

  resetPassword(userEmail,userPassword){
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateOne({'email' : String(userEmail)},{ $set : { 'password' : userPassword }},(err,result) => {
          if(err){
            reject(err)
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  getStudentsById(list){
    list = list.map((id) => {
      return new MongoDb.ObjectId(id);
    });

    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({'_id' : {$in : list}}).toArray((err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  getClassesById(list){
    list = list.map((id) => {
      return new MongoDb.ObjectId(id);
    });

    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({'_id' : {$in : list}}).toArray((err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }


  addStudentToClass(studentId,classID){
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateOne({'_id' : new MongoDb.ObjectId(classID)},{$push : {'studentList' : studentId}},(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  updateClass(updatedClass){
    updatedClass._id = new MongoDb.ObjectId(updatedClass.id);
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.replaceOne({'_id' : updatedClass._id},updatedClass,(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result.ops[0]);
          }
        })  
      })
    })
  }

  removeStudentFromClass(studentId,classId){
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateOne({'_id' : MongoDb.ObjectId(classId)},{ $pull : { 'studentList' : studentId}},(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  deleteClass(classId){
    return this.getCollection('classes')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.deleteOne({'_id' : new MongoDb.ObjectId(classId)},(err,result) => {
          if(err){
            console.log('in mongo, error while deleting a class in mongo');
            reject(err);
          }
          else{
            resolve('deleting class completed');
          }
        })
      })
    })
  }

  getVideos(){
    return this.getCollection('videos')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({}).toArray((err,result) => {
          if(err){
            console.log('error occured in mongo while getting videos');
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    })
  }

  getCollection(collectionName){
    this.dbCollections[collectionName] = this.dbCollections[collectionName] || new Promise((resolve,reject) => {
      this.connectToDB()
      .then((db) => {
        let collection = db.collection(collectionName);
        resolve(collection);
      })
      .catch((reason) => {
        console.log("Error: cannot get collection",reason);
        reject(reason);
      });
    });
    return this.dbCollections[collectionName];    
  }
  
  editStudent(student){
    return this.getStudentsById([student.id])
    .then((dbStudent)=>{
      student.password = dbStudent.password;
      return this.getCollection('students')
      .then((collection) => {
        return new Promise((resolve,reject) => {
          collection.replaceOne({'_id' : MongoDb.ObjectId(student.id)},student,(err,result) => {
            if(err){
              console.log('error in mongo in updating student')
              reject(err);
            }
            else{
              console.log('student updated in mongo')
              resolve(this.getStudetList())
            }
          })
        })
      })
    })
  }

  updateStudentClassList(studentList,classId){
    studentList = studentList.map(id => new MongoDb.ObjectId(id));
    return this.getCollection('students')
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.updateMany({'_id' : { $in : studentList}},{$addToSet : {'enrolledClasses' : classId}},(err,result) => {
          if(err){
            reject(err)
          }
          else{
            console.log('updating done')
            resolve(result);
          }
        })
      })
    })
  }

  addStudent(student){
   return this.getCollection("students")
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.insertOne(student,null,(err,result) => {
          if(err){
            console.log("can't upload student to mongo")
            reject(err);
          }
          else{
            console.log("done")
            resolve(this.getStudetList());
          }
        })
      })
    })
  }
  
  deleteStudent(student){
   return this.getCollection("students")
    .then((collection) => {
      return new Promise((resolve, reject) => {    
        collection.deleteOne({"lName" : student.lName,"fName" : student.fName, 'email' : student.email },(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(this.getStudetList());
          }
        });
      });
    });
  }

  deleteAll(){
    return this.getCollection("students")
    .then((collection) => {
      return new Promise((resolve, reject) => {    
        collection.deleteMany({},(err,result) => {
          if(err){
            reject(err);
          }
          else{
            resolve(this.getStudetList());
          }
        });
      });
    });
  }

  getStudetList(){
    return this.getCollection("students")
    .then((collection) => {
      return new Promise ((resolve,reject) => {
        collection.find({}).toArray((err,result) => {
          if(err){
            console.log("can't get Students from mongo");
            reject(err);
          }
          else{
            resolve(result);
          }
        })
      })
    }).catch((err) => {
      console.log("can't get studemt list");
    });
  }

  authenticateUser(username,password){
    this.connectToDB()
    .then((db) => {
      MongoDb.auth(username,password);
    })
  }
}

exports.MongoConnector = MongoConnector;
  