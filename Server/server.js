"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongo = require('./MongoServer');
const jwt = require('jsonwebtoken');
const transporter = require('nodemailer').createTransport({
    service: 'gmail',
    auth: {
      user: 'no.reply.rpd@gmail.com',
      pass: '$RPD@admin$'
    }
  });
var mailOptions = {
    from: 'no.reply.rpd@gmail.com',
    subject: 'Password reset request'
};
const app = express();
const url = 'mongodb://dsingh35:Davinder463@ds249824.mlab.com:49824/rpd';
const dbName = 'rpd';
const mongoConnector = new mongo.MongoConnector(url,dbName);
const counter = [];

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 
};
app.use(cors(corsOptions));
app.use(bodyParser.json({limit: '10mb', extended: true}))

app.param('jwt', function(req,res,next,value){
    req.data = req.data || {};
    req.data.name = value;
    next();
});

function getToken(req,res,next){
    const bToken = req.headers['authorization'];
    if(bToken){
        const token = bToken.split(' ')[1];
        req.token = token;
        next();
    }
    else{
        console.log('failed to pass getToken');
    }
}

app.post('/verifyToken',getToken, async (req,res) => {
    let authData = verifyToken(req.token);
    if(authData){
        res.json({email : authData.email});
    }
    else{
        res.json(403,'token is invalid');
    }
})

app.post('/resetPassword',getToken,(req,res) => {
    let tokenData = verifyToken(req.token);
    if(tokenData){
        mongoConnector.resetPassword(tokenData.email,req.body.password)
        .then((result) => {
            res.json(result)
        }).catch((err) => {
            res.json(500,err);
        });
    }
    else{
        res.json(403,'token is invalid')
    }
})


app.post('/deleteNotification',getToken,(req,res) => {
    let user = verifyToken(req.token);
    if(user){
        mongoConnector.deleteNotification(user._id,req.body.notificationId)
        .then((result) => {
            res.json(result)
        }).catch((err) => {
            res.status(500).json('failed to delete the notification')
        });
    }
    else{
        res.status(403).json('forbidden');
    }
})

app.post('/markNotificationRead',getToken,(req,res) => {
    let user = verifyToken(req.token);
    if(user){
        mongoConnector.markNotificationRead(user._id,req.body.notificationId)
        .then((result) => {
            res.json(result)
        }).catch((err) => {
            res.status(500).json(err)
        });
    }
    else{
        res.status(403).json('forbidden');
    }
})

app.post('/saveNotification',getToken,async (req,res) => {
    let idExists;
    await mongoConnector.checkIdExists(req.body.studentId)
    .then((result) => {
        idExists = true;

    }).catch((err) => {
        idExists = false;
    });

    let admin = isadmin(req.token);

    if(!idExists || !admin){
        admin = await mongoConnector.getAdminId();
        req.body.studentId = admin.id; 
    }

    if(!req.body.notification.fName && admin){
        req.body.notification = {
            fName : admin.fName,
            lName : admin.lName,
            email : admin.email,
            phone : admin.phone,
            details : req.body.notification,
            address : admin.address
        }
    }
    req.body.notification.status = 'unread';
    mongoConnector.saveNotification(req.body.studentId,req.body.notification)
    .then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(500).json(err);
    });
})

app.post('/getNotifications',(req,res) => {
    mongoConnector.getNotifications(req.body.collectionName)
    .then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(500).json(err);
    });
})

app.post("/deleteStudent",getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.deleteStudent(req.body)
        .then((result) => {
        res.send({ students : result});   
        }).catch((err) => {
            console.log("deleting student from mongo failed",err);
        });
    }
    else{
        res.sendStatus(403);
    }
});

app.post('/createClass',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.createClass(req.body)
        .then((result) => {
            res.send({'newClass' : result})
        }).catch((err) => {
            console.log('error has occured while creating classs',err);
            res.status(500);
            res.send({error : err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/class/addStudent',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.addStudentToClass(req.body.studentId,req.body.classId)
        .then((result) => {
            res.send({'newStudent' : result});
        }).catch((err) => {
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/class/removeStudent',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.removeStudentFromClass(req.body.studentId,req.body.classId)
        .then((result) => {
            res.send({'result' : result});
        }).catch((err) => {
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/getStudentsById',getToken,(req,res) => {
    if(isProfileOwner(req.token,req.body.list)){
        mongoConnector.getStudentsById(req.body.list)
        .then((result) => {
            res.send({'studentList' : result})
        }).catch((err) => {
            console.log('failed to get')
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403)
    }
})

app.post('/getStudentsByIdForClass',getToken,(req,res) => {
    if(verifyToken(req.token)){
        mongoConnector.getStudentsById(req.body.list)
        .then((result) => {
            result = result.map((student) => {
                return {'fName' : student.fName, 'lName' : student.lName, 'id' : student._id};
            });
            res.json(result)
        }).catch((err) => {
            console.log('failed to get')
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403)
    }
})

function isStudentEnrolled(token,list){
    try{
        let user = verifyToken(token)
        let enrolledStatus = true;
        list.forEach(element => {
            if(!user.enrolledClasses.includes(element)){
                enrolledStatus = false;
            }
        });
        return user.role == 'RPDadmin' || enrolledStatus;
    }
    catch(err){
        console.log('error has occured')
        return false;
    }
}

app.post('/getClassesById',getToken,(req,res) => {
    if(isStudentEnrolled(req.token,req.body.list)){
        mongoConnector.getClassesById(req.body.list)
        .then((result) => {
            res.json(result)
        }).catch((err) => {
            console.log('failed to get classes by id')
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403)
    }
})

app.post('/updateClass',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.updateClass(req.body)
        .then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(500);
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/student/logIn',(req,res) => {
    mongoConnector.logIn(req.body)
    .then((result) => {
        delete result.img;
        jwt.sign({user : result},'secretKey',(err,token) => {
            res.send({'token' : token, 'user' : {id : result._id, email : result.email , role : result.role,enrolledClasses : result.enrolledClasses}});
        })
    }).catch((err) => {
        res.status(500);
        res.send(err);
    });
})

app.post('/deleteClass',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.deleteClass(req.body.id)
        .then((result) => {
            console.log('class deleted')
            res.send({'status' : result});
        }).catch((err) => {
            console.log('error, failed to delete class',err);
            res.send({'error' : err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.get('/getClasses',getToken,(req,res) => {
    if(!isadmin(req.token)){
        res.sendStatus(403);
    }
    else{
        mongoConnector.getClasses()
        .then((result) => {
            res.send({'classes' :result})
        }).catch((err) => {
            console.log('failed to get all the classes from mongo');
            res.status(500);
            res.send(err)        
        });
    }
})

app.get('/',(req,res) => {
    res.send('hello');
})

app.get('/getGallery',(req,res) => {
    console.log('35')
    mongoConnector.getPictures()
    .then((result) => {
        console.log('getting images from mongo line 39');  
        mongoConnector.getVideos()
        .then((vid) => {
            res.send({images : result, videos : vid})
        })
        .catch((err) => {
            console.log('line 43')
            res.status(500);
            res.send(err);
        })
    }).catch((err) => {
        console.log('error occured in mongo while getting images',err);
        res.status(500);
        res.send({error : err});
    });
})

app.post('/saveImage',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.saveImage(req.body)
        .then((result) => {
            console.log('in server, image is successfully posted in mongo')
            res.send({image : result})
        }).catch((err) => {
            console.log("error occured in mongo while saving image",err);
            res.status(500);
            res.send({error: err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/deleteVideo',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.deleteVideo(req.body.id)
        .then((result) => {
            res.json(result)
        }).catch((err) => {
            res.json(500,'failed to delete video')
        });
    }
    else{
        res.sendStatus(403);
    }
})

app.post('/saveVideo',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.saveVideo(req.body)
        .then((result) => {
            console.log('in server, Video is successfully posted in mongo')
            res.json(result);
        }).catch((err) => {
            console.log("error occured in mongo while saving video",err);
            res.json(500,err);
        });
    }
    else{
        res.sendStatus(403);
    }
})

function verifyToken(token){
    return jwt.verify(token,'secretKey',(err,authData) => {
        if(err){
            console.log('token failed verifiaction')
            return null;
        }
        else{
            return authData.user;
        }
    })
}

app.post('/getResetLink',(req,res) => {
    mailOptions.to = req.body.email;
    req.body.email && jwt.sign({user : {email : req.body.email}},'secretKey',{expiresIn : 6000000},(err,token) => {
       mailOptions.html = `<a href="http://localhost:4200/resetPassword/${token}">click here to reset your password</a>`
        transporter.sendMail(mailOptions,(er,info) => {
            if(err){
                res.sendStatus(500);
            }
            else{
                res.send({'result' : 'mail has been sent to your email'});
            }
        })
    })
})


app.get("/getStudentList",getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.getStudetList()
        .then((result) => {
            //console.log(result)
            res.send({students : result})
        })
        .catch((err) => {
            console.log("error in getting student list from mongo");
            res.status(500);
            res.send(err);
        })
    }
    else{
        res.sendStatus(403);
    }
})



function isProfileOwner(token,ids){
    try{
        let user = verifyToken(token);
        return (ids.length == 1 && user._id == ids[0]) || user.role == 'RPDadmin';
    }
    catch(err){
        return false;
    }
}

app.post('/checkEmailExists',(req,res) => {
    mongoConnector.checkEmailExists(req.body.email)
    .then((result) => {
        res.json(result)
    }).catch((err) => {
        console.log('eror in checking email');
        res.sendStatus(500);
    });
})

app.post('/updateStudentClassList',getToken,(req,res) => {
    if(isadmin(req.token)){
        mongoConnector.updateStudentClassList(req.body.studentList,req.body.id)
        .then((result) => {
            res.json(result);
        }).catch((err) => {
            console.log('failed to update student student list',err)
            res,sendStatus(500);
        });
    }
})

app.post("/editStudent",getToken,(req,res) => {
    console.log('student edititng',req.body)
    if(isProfileOwner(req.token,[req.body])){
        mongoConnector.editStudent(req.body)
        .then((response)=>{
            res.send({students : response})
        })
        .catch((err) => {
            console.log('error in server in editing student')
            res.status(500);
            res.send({error : err})
        })
    }
    else{
        res.sendStatus(403);
    }
})

function isadmin(token){
    try{
        let user = verifyToken(token);
        if(user.role == 'RPDadmin'){
            return user;
        }
    }
    catch(err){
        console.log('not authorized');
        return false;        
    }
}

app.post("/addStudent",getToken,(req,res) => {
    if(isadmin(req.token)){
        req.body.password = 'secret';
        mongoConnector.addStudent(req.body)
        .then((result) => {
        res.send({ students : result});   
        }).catch((err) => {
            console.log("adding student to mongo failed",err);
            res.status(500);
            res.send({error: err});
        });
    }
    else{
        res.sendStatus(403)
    }
})

app.post("/deletePicture",getToken,(req,res) => {
    if(isadmin){
        mongoConnector.deleteImage(req.body.id)
        .then((result) => {
            console.log('done deleting in server')
            res.send({'deleting image from server' : 'done'});
        }).catch((err) => {
            console.log('failed deleting image in server')
            res.status(500);
            res.send({error: err});
        });
    }
    else{
        res.sendStatus(403);
    }
})

function getCounter(counterName){
    console.log('in the getCointer func')
    mongoConnector.getCounter(counterName)
    .then((count) => {
        counter[counterName] = count.count;
        console.log('got the counter', counter[counterName])
    })
    .catch((reason) => {
        console.log(`error in getting ${counterName} count`);
    })
}

app.listen(8080, function () {
    console.log("server started");
    // rl.question('',(answer) => {
    //     this.username = answer
    //     rl.question('',(answer) => {
    //         this.password =  answer;
    //         console.log('username ad password are',this.username,this.password)
    //         mongoConnector.authenticateUser(this.username,this.password);
    //         rl.close();
    //     });
    // });
    
    //getCounter('pictures');
    //mongoConnector.cleanGallery();
});
